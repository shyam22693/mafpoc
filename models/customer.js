'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var customerSchema = new Schema({
	agentcode: { type: String},
	inscompanyname: { type: String},
	product: { type: String},
	customername: { type: String},
	dob: { type: String},
	premiumfreq: { type: String},
	premiumamount: { type: Number},
	yearpremium: { type: Number},
	agentcommission: { type: Number},
	companypool: { type: Number},
	insuranceid: { type: String}
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }, toObject: { virtuals: true }, toJSON: { virtuals: true } });

module.exports = mongoose.model('Customer', customerSchema);

