var app = angular.module('pocApp', []);

// app.directive('format', ['$filter', function ($filter) {
//     return {
//         require: '?ngModel',
//         link: function (scope, elem, attrs, ctrl) {
//             if (!ctrl) return;

//             ctrl.$formatters.unshift(function (a) {
//                 return $filter(attrs.format)(ctrl.$modelValue)
//             });

//             elem.bind('blur', function(event) {
//                 var plainNumber = elem.val().replace(/[^\d|\-+|\.+]/g, '');
//                 elem.val($filter(attrs.format)(plainNumber));
//             });
//         }
//     };
// }]);

app.controller('customerCtrl', function($scope,$log,$timeout) {
	$scope.frequency = "";
    $scope.agenterr = "";
    $scope.err1 = false;
    $scope.err2 = false;
    $scope.datas = true;
    $scope.allow = true;

    $scope.clock = new Date();
      // $scope.clock = "YYYY-MM-DD / _ _ : _ _ : _ _";
      // $scope.tickInterval = 1000
      // var tick = function() {
      //   $scope.clock = new Date();
      //   $timeout(tick, $scope.tickInterval);
      // }
      // $timeout(tick, $scope.tickInterval);

	$scope.agentvalidation = true;
    $scope.agents = [{"agentcode":"AAA31","name":"AAA31"},
                     {"agentcode":"AAA32","name":"AAA32"},
                     {"agentcode":"AAA33","name":"AAA33"},
                     {"agentcode":"AAA34","name":"AAA34"},
                     {"agentcode":"AAA35","name":"AAA35"},
                     {"agentcode":"AAA36","name":"AAA36"},
                     {"agentcode":"AAA37","name":"AAA37"},
                     {"agentcode":"AAA38","name":"AAA38"},
                     {"agentcode":"AAA39","name":"AAA39"},
                     {"agentcode":"AAA40","name":"AAA40"},
                     {"agentcode":"AAA41","name":"AAA41"},
                     {"agentcode":"AAA42","name":"AAA42"},
                     {"agentcode":"AAA43","name":"AAA43"},
                     {"agentcode":"AAA44","name":"AAA44"},
                     {"agentcode":"AAA45","name":"AAA45"},
                     {"agentcode":"AAA46","name":"AAA46"},
                     {"agentcode":"AAA47","name":"AAA47"},
                     {"agentcode":"AAA48","name":"AAA48"},
                     {"agentcode":"AAA49","name":"AAA49"},
                     {"agentcode":"AAA50","name":"AAA50"},
                     {"agentcode":"AAA51","name":"AAA51"},
                     {"agentcode":"AAA52","name":"AAA52"},
                     {"agentcode":"AAA53","name":"AAA53"},
                     {"agentcode":"AAA54","name":"AAA54"},
                     {"agentcode":"AAA55","name":"AAA55"}
                    ]

    $scope.insurancecompany = [
                              {"companyname":"Transamerica","products":["T1","T2","T3","T4","T5"]},
                              {"companyname":"Nationwide","products":["N1","N2","N3","N4","N5"]},
                              {"companyname":"Pacific Life","products":["P1","P2","P3","P4","P5"]},
                              ];

    $scope.companyproducts = [];


$scope.$watchGroup(['agentcode','inscompanyname','product','insuranceid','premiumfreq','premiumamount'], function (agent){
$log.info(agent);
// console.log('agent',agent);
if(agent[0]!=undefined && agent[0].length>0)
{
  if(agent[1]!=undefined && agent[1].length>0)
  {
    // console.log('ccccccccccccc',agent[1],agent[1].length);
    $scope.outinscompanyname = agent[1];
    // $scope.datas = true;

    if(agent[2]!=undefined && agent[2].length>0)
    {
      $scope.outproduct = agent[2];

      if(agent[3]!=undefined && agent[3].length>0)
      {
        $scope.outinsuranceid = agent[3];
        $scope.outcreated_at = new Date();

        if(agent[4]!=undefined && agent[4].length>0)
        {
          $scope.outpremiumfreq = agent[4];

          if(agent[5]!=undefined && agent[5].length>0)
          {
            if(agent[5].match(/^[0-9.]+$/))
            {
                $scope.outpremiumamount = parseFloat(agent[5]);
                $scope.outyearpremium = $scope.outpremiumfreq == "weekly" ? parseFloat(agent[5])*52:$scope.outpremiumfreq == "monthly"? parseFloat(agent[5])*12 : parseFloat(agent[5]);
                $scope.outagentcommission = ($scope.outyearpremium*80)/100;
                $scope.outcompanypool = $scope.outyearpremium-$scope.outagentcommission;
                $scope.agentvalidation = $scope.allow == true?true:false;
                $scope.errmessage = $scope.agentvalidation == false? "Click on Submit!":"Agent Verification failed";
            }
            else
            {
                $scope.errmessage = "Premium Amount Should be Number!";
                $scope.agentvalidation = true; 
                $scope.outpremiumamount = $scope.outyearpremium = $scope.outagentcommission = $scope.outcompanypool=undefined;
            }
          }
          else
          {
              $scope.errmessage = "Enter Premium Amount!";
              $scope.agentvalidation = true;
              $scope.outpremiumamount = $scope.outyearpremium = $scope.outagentcommission = $scope.outcompanypool=undefined;
          }
        }
        else
        {
            $scope.errmessage = "Choose Premium Frequency!";
            $scope.agentvalidation = true;
            $scope.outpremiumamount = $scope.outyearpremium = $scope.outagentcommission = $scope.outcompanypool=$scope.premiumamount=$scope.outpremiumfreq=undefined;
        }
      }
      else
      {
          $scope.errmessage = "Enter Policy Number!";
          $scope.agentvalidation = true;
          $scope.outinsuranceid = "";
      }
    }
    else
    {
        $scope.errmessage = "Choose the Product!";
        $scope.agentvalidation = true;
        $scope.outproduct = undefined;
    }
  }
  else
  {
      $scope.errmessage = $scope.err2==true?"Choose Company Name!":"Invalid Agent Please Check!";
      $scope.agentvalidation = true;
      $scope.outinscompanyname = undefined;
      $scope.outproduct = undefined;
      $scope.product = undefined;
      // $scope.datas = false;
  }
}
else
{
    $scope.errmessage = "Agent Code Verification Required!";
    $scope.agentvalidation = $scope.allow == true?true:false;
}
});

    $scope.companySelect = function()
    {
        // console.log('inscompanyname',$scope.inscompanyname);
        $scope.insurancecompany.map(c=>{
            // console.log(c);
            if(c.companyname === $scope.inscompanyname)
            {
                $scope.companyproducts = c.products;
            }
        })
        // $scope.companyproducts

    };
    $scope.agentcodecheck = function()
    {
    	// console.log('checking',$scope.agentcode.length);
    	if($scope.agentcode.length!=0)
    	{
            $scope.agenterr = "No agent Available";
            $scope.err1 = true;
    		$scope.err2 = false;
            $scope.allow = true;
    		$scope.agents.map(x=>{
    			// console.log('x',x.agentcode);
    			if($scope.agentcode == x.agentcode)
    			{
    				$scope.agenterr = "agent Available go on";
                    $scope.allow = false;
                    $scope.err2 = true;
                    $scope.err1 = false;
    			}
    		})
    	}
    	else
    	{
    		$scope.agenterr = "";
    	}
    }

    $scope.freq = function()
    {
    	// console.log($scope.premiumfreq);
    	$scope.frequency = $scope.premiumfreq
    }
});