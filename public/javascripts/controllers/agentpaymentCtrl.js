var app = angular.module('pocApp', []);


app.filter('dateRange', function() {
    //console.log('shyam');
        return function( items, fromDate, toDate ) {
            var filtered = [];
            //console.log('iam at',Date.parse(fromDate), Date.parse(toDate));
            angular.forEach(items, function(item) {
                //console.log('item',Date.parse(item.created_at),item);
                if(Date.parse(item.created_at) >= Date.parse(fromDate) && Date.parse(item.created_at) <= Date.parse(toDate)) {
                    filtered.push(item);
                }
                //console.log('filtered',filtered)
            });
            return filtered;
        };
    });


app.controller('agentpaymentCtrl', function($scope,$log,$http,$filter)
{
    $scope.agenterr = "";
    $scope.datedis = true;
    $scope.dataerr = false;
    $scope.totalpay = 0;
    $scope.totalpool = 0;
    $scope.angentstatus = false;
    $scope.agentpayments = [];
    $scope.agents = [{"agentcode":"AAA31","name":"AAA31"},
                     {"agentcode":"AAA32","name":"AAA32"},
                     {"agentcode":"AAA33","name":"AAA33"},
                     {"agentcode":"AAA34","name":"AAA34"},
                     {"agentcode":"AAA35","name":"AAA35"},
                     {"agentcode":"AAA36","name":"AAA36"},
                     {"agentcode":"AAA37","name":"AAA37"},
                     {"agentcode":"AAA38","name":"AAA38"},
                     {"agentcode":"AAA39","name":"AAA39"},
                     {"agentcode":"AAA40","name":"AAA40"},
                     {"agentcode":"AAA41","name":"AAA41"},
                     {"agentcode":"AAA42","name":"AAA42"},
                     {"agentcode":"AAA43","name":"AAA43"},
                     {"agentcode":"AAA44","name":"AAA44"},
                     {"agentcode":"AAA45","name":"AAA45"},
                     {"agentcode":"AAA46","name":"AAA46"},
                     {"agentcode":"AAA47","name":"AAA47"},
                     {"agentcode":"AAA48","name":"AAA48"},
                     {"agentcode":"AAA49","name":"AAA49"},
                     {"agentcode":"AAA50","name":"AAA50"},
                     {"agentcode":"AAA51","name":"AAA51"},
                     {"agentcode":"AAA52","name":"AAA52"},
                     {"agentcode":"AAA53","name":"AAA53"},
                     {"agentcode":"AAA54","name":"AAA54"},
                     {"agentcode":"AAA55","name":"AAA55"}
                    ]

     $scope.$watchGroup(['agentname'], function (dates) {
         $log.info(dates);
            if (dates[0]!=undefined && dates[0]!=""){
                $scope.datedis = false;
            }
        });

        $scope.reset = function()
        {
            window.location.reload();
        }
    // $scope.getPayments = function()
    // {
    //     console.log('checking');
    //     console.log('checking',$scope.agentname);
    //     $scope.totalpay = 0;
    //     $scope.totalpool = 0;
    //     $scope.from_date = '';
    //     $scope.to_date = '';
    //     $http.post('/getpayments',{agent:$scope.agentname}).then(function(res){
    //         if(res.data)
    //         {
    //             console.log('data data',res.data);
    //             $scope.agentpayments = res.data;
    //             if($scope.agentpayments.length!=0)
    //             {
    //                 $scope.angentstatus = true;
    //                 $scope.agentpayments.map(x=>{
    //                     console.log(x.agentcommission);
    //                     $scope.totalpay += parseFloat(x.agentcommission);
    //                     $scope.totalpool += parseFloat(x.companypool);
    //                     console.log($scope.totalpay)
    //                     console.log($scope.totalpool)
    //                 })
    //             }
    //             else
    //             {
    //                 $scope.angentstatus = false;
    //                 $scope.totalpay = 0;
    //                 $scope.totalpool = 0;
    //             }
    //         }
    //     })
    // }

    $scope.getDateFilter = function()
    {
             // console.log("get get",$scope.from_date,$scope.to_date,$scope.agentname)
                $scope.totalpay = 0;
                $scope.totalpool = 0;
                $http.post('/getpayments',{agent:$scope.agentname}).then(function(res){
                    if(res.data)
                    {
                        //console.log('data data',res.data);
                        $scope.agentpayments = res.data;
                        if($scope.agentpayments.length!=0)
                        {
                            //console.log('sssssss',$scope.from_date,$scope.to_date)
                            $scope.dataerr = false;
                            if($scope.from_date!=undefined && $scope.to_date!=undefined)
                            {
                               $scope.agentpayments = $filter('dateRange')($scope.agentpayments,$scope.from_date,$scope.to_date);
                                if($scope.agentpayments.length!=0)
                                {
                                    $scope.dataerr = false;
                                    $scope.angentstatus = true;
                                    $scope.agentpayments.map(x=>{
                                    //console.log(x.agentcommission);
                                    $scope.totalpay += parseFloat(x.agentcommission);
                                    $scope.totalpool += parseFloat(x.companypool);
                                    //console.log($scope.totalpay)
                                    //console.log($scope.totalpool)
                                    })
                                }
                                else
                                {
                                    $scope.agentdatarr = "No Policy Holders Avaliable On Selected Range Dates!";
                                    $scope.dataerr = true;
                                    $scope.angentstatus = false;
                                    $scope.totalpay = 0;
                                    $scope.totalpool = 0;
                                }
                            }
                            else
                            {
                                $scope.angentstatus = true;
                                $scope.dataerr = false;
                                $scope.agentpayments.map(x=>{
                                    //console.log(x.agentcommission);
                                    $scope.totalpay += parseFloat(x.agentcommission);
                                    $scope.totalpool += parseFloat(x.companypool);
                                    //console.log($scope.totalpay)
                                    //console.log($scope.totalpool)
                                })
                            }
                        }
                        else
                        {
                            $scope.angentstatus = false;
                            $scope.totalpay = 0;
                            $scope.totalpool = 0;
                            $scope.agentdatarr = "No Policy Holders Avaliable By This Agent";
                            $scope.dataerr = true;
                        }
                    }
                })
    }
});
