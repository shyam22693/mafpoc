var express = require('express');
var router = express.Router();
var Customer = require('../models/customer');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('inscompany/addcustomer');
});

router.get('/payments', function(req, res, next) {
  res.render('inscompany/agentPayment');
});

router.post('/getpayments', function(req, res, next) {
  // console.log(req.body);
  Customer.find({agentcode:req.body.agent}).exec(function(err,data){
  	if(!err)
  	{
  		// console.log('data',data);
  		res.send(data);
  	}
  	else
  	{
  		console.log('err',err);
  	}
  })
});

// function makeid() {
//   var text = "";
//   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

//   for (var i = 0; i < 10; i++)
//     text += possible.charAt(Math.floor(Math.random() * possible.length));

//   return text;
// }

router.post('/savecustomer', function(req, res, next){
	// console.log('req.body',req.body);
	if(req.body.premiumfreq =="weekly")
	{
		req.body.yearpremium = parseFloat(req.body.premiumamount)*52;
	}
	else if(req.body.premiumfreq =="monthly")
	{
		req.body.yearpremium = parseFloat(req.body.premiumamount)*12;
	}
	else if(req.body.premiumfreq =="yearly")
	{
		req.body.yearpremium = parseFloat(req.body.premiumamount);
	}
	else
	{
		req.body.yearpremium = parseFloat(req.body.premiumamount);
	}
	req.body.agentcommission = (req.body.yearpremium*80)/100;
	req.body.companypool = parseFloat(req.body.yearpremium) - (req.body.yearpremium*80)/100;
	// req.body.insuranceid = makeid();

  	var newCustomer = new Customer();  
      newCustomer.agentcode = req.body.agentcode ? req.body.agentcode : ""; 
      newCustomer.inscompanyname = req.body.inscompanyname ? req.body.inscompanyname : "";
      newCustomer.product = req.body.product ? req.body.product : "";
	  // newCustomer.customername = req.body.customername ? req.body.customername : "";
	  // newCustomer.dob = req.body.dob ? req.body.dob : "";
	  newCustomer.premiumfreq = req.body.premiumfreq ? req.body.premiumfreq : "";
	  newCustomer.premiumamount = req.body.premiumamount ? req.body.premiumamount : "";
	  newCustomer.yearpremium = req.body.yearpremium ? req.body.yearpremium : "";
	  newCustomer.agentcommission = req.body.agentcommission ? req.body.agentcommission : "";
	  newCustomer.companypool = req.body.companypool ? req.body.companypool : "";
	  newCustomer.insuranceid = req.body.insuranceid ? req.body.insuranceid : "";
      newCustomer.save((err, customer) => {
        if (!err) {
          res.redirect("/payments");
        } else {
          res.redirect("/payments");
        };
      });
});


module.exports = router;
